******************************************************************************
FAQ - Modifikation zur Suche von Credentials mit Hilfe von GitLeaks
******************************************************************************

.. sectionauthor:: Support <support@ndaal.eu>
   
.. contents:: Inhalt - Modifikation zur Suche von Credentials mit Hilfe von GitLeaks
   :depth: 3

.. _Section_ndaal_-_Modifikation_zur_Suche_von_Credentials_von_GitLeaks:

Allgemeines
------------------------------------------------------------------------------

**GitLeaks** [1]_ ist ein Tool zur statischen Prüfung der Sicherheit einer Anwendung (SAST, engl.: **S**\ tatic **A**\ pplication **S**\ ecurity **T**\ esting). Es dient dem **Erkennen** und **Vermeiden** von hart kodierten Geheimnissen wie Passwörtern, API-Schlüsseln und Token in Git-Repos. Ndaal hat einen GitLeaks-Regelsatz erstellt, der auf den RegEx-Asudrücken basiert, die in `verschiedenen Quellen <Quellen auf denen das Regelwerk beruht_>`_ gefunden wurden.
Der Regelsatz, der mit GitLeaks verwendet werden soll, liegt im TOML-Format vor. Die gleichen Informationen sind jedoch auch im JSON-Format bereitgestellt.

Installation
------------------------------------------------------------------------------

**GitLeaks** basiert auf **GoLang** [2]_ wie nachfolgend gezeigt installiert werden (siehe GitLeaks installation documentation [3]_ ):

    .. code-block:: shell-session

        # MacOS
        brew install gitleaks

        # Docker (DockerHub)
        docker pull zricethezav/gitleaks:latest
        docker run -v ${path_to_host_folder_to_scan}:/path zricethezav/gitleaks:latest [COMMAND] --source="/path" [OPTIONS]

        # Docker (ghcr.io)
        docker pull ghcr.io/zricethezav/gitleaks:latest
        docker run -v ${path_to_host_folder_to_scan}:/path zricethezav/gitleaks:latest [COMMAND] --source="/path" [OPTIONS]

        # From Source
        git clone https://github.com/zricethezav/gitleaks.git
        cd gitleaks
        make build

Benutzung
------------------------------------------------------------------------------

Nachdem GitLeaks erfolgreich installiert worden ist, können weitere Informationen zur Benutzung der Hilfe entnommen werden, welche mittles :code:`gitleaks -h` aufgerufen werden kann und folgende Ausgabe hat:

    .. code-block:: shell-session

        Gitleaks scans code, past or present, for secrets

        Usage:
        gitleaks [command]

        Available Commands:
        completion  generate the autocompletion script for the specified shell
        detect      detect secrets in code
        help        Help about any command
        protect     protect secrets in code
        version     display gitleaks version

        Flags:
        -b, --baseline-path string       path to baseline with issues that can be ignored
        -c, --config string              config file path
                                        order of precedence:
                                        1. --config/-c
                                        2. env var GITLEAKS_CONFIG
                                        3. (--source/-s)/.gitleaks.toml
                                        If none of the three options are used, then gitleaks will use the default config
            --exit-code int              exit code when leaks have been encountered (default 1)
        -h, --help                       help for gitleaks
        -l, --log-level string           log level (trace, debug, info, warn, error, fatal) (default "info")
            --max-target-megabytes int   files larger than this will be skipped
            --no-banner                  suppress banner
            --redact                     redact secrets from logs and stdout
        -f, --report-format string       output format (json, csv, sarif) (default "json")
        -r, --report-path string         report file
        -s, --source string              path to source (default: $PWD) (default ".")
        -v, --verbose                    show verbose output from scan

        Use "gitleaks [command] --help" for more information about a command.

Für die Erkennung von Geheimnissen wird der Befehl :code:`detect` verwendet. Um ihn mit der ndaal-Konfiguration zu verwenden, kann er wie folgt aufgerufen werden:

    .. code-block:: shell

        gitleaks -c <path to ndaal gitleaks.toml> detect -v <path to repo to be scanned>

Der obige Befehl gibt die Ergebnisse auf dem Standard Output (stdout) aus. Wenn aus der Ausgabe jedoch ein Bericht erstellt werden soll, kann dies mit der Flag :code:`-r` geschehen, um den Pfad anzugeben, der zur Erstellung des Berichts verwendet wird. Mit :code:`-f`, um das Format des Berichts anzugeben. Hier sind die Möglichkeiten:
    - json
    - csv
    - sarif

Der Standard ist ein JSON-Bericht zu erstellen.

List der Regeln
------------------------------------------------------------------------------

Die nachfolgende Liste gibt einen Überblick über die Art der Credentials, welche mit Hilfe des von ndaal erstellten Regelwerks gefunden werden können:

- Google Speicher API
- Google Speicher Domain
- Appspot Domain
- Cloudfront Domain
- DigitalOcean Domain
- AWS
- AWS MWS Schlüssel
- AWS Manager ID
- AWS geheimer Schlüssel
- AWS Credentials Datei Information
- Adobe Client ID (Oauth Web)
- Adobe Client Secret
- Age geheimer Schlüssel
- Alibaba Zugangsschlüssel ID
- Alibaba geheimer Schlüssel
- Amazon SNS Thema
- App SID für Twilio
- Asana Client ID
- Asana Client Secret
- Atlassian API-Token
- Beamer API-Token
- Bitbucket Client ID
- Bitbucket Client Secret
- Bitcoin Addresse
- Bitly geheimner Schlüssel
- Clojars API Token
- Cloudinary Credentials
- Contentful Delivery API Token
- Credentials innerhalb einer URI
- Databricks API Token
- Discord API Schlüssel
- Discord Webhook
- Discord Client ID
- Discord Client Secret
- Doppler API Token
- Dropbox API Geheimnis/Schlüssel
- Dropbox langlebiger API Token
- Dropbox kurzlebiger API Token
- Duffel API Token
- Dynatrace API Token
- EasyPost API Token
- EasyPost Test API Token
- Verschlüsselte Passwörter in Dateien unter Linux
- Facebook Zugriffs-Token
- Facebook Client ID
- Facebook geheimer Schlüssel
- Facebook Token
- Fastly API Token
- Dateiendung, welche üblicherweise Credentials enthält
- Datei-Namen, welche üblicherweise Credentials enthalten
- Finicity API Token
- Finicity Client Secret
- Flutterwave verschlüsselter Schlüssel
- Flutterwave öffentlicher Schüssel
- Flutterwave geheimer Schlüssel
- Frame.io API Token
- Generischer API Schlüssel
- Generischer API Schlüssel in Windows
- Generisches zitiertes Credential
- Generisches unzitiertes Credential
- GitHub App Token
- GitHub OAuth Zugriffs Token
- GitHub persönlicher Zugriffs Token
- GitHub Refresh Token
- GitHub
- GitLab persönlicher Zugriffs Token
- GoCardless API Token
- Google (GCP) Service-account
- Google API Schlüssel
- Google Calendar URI
- Google Captcha
- Google Cloud Platform API Schlüssel
- Google FCM Server Schlüssel
- Google OAuth Zugriffs-Token
- Google OAuth ID
- Grafana API Token
- HTTP-Header welcher Basic-Authorisierung enthält
- HTTP-Header welcher Bearer-Authorisierung enthält
- Hardcoded Credentials in Go Dateien
- Hardcoded Credentials in HCL Dateien (:code:`*.tf`)
- Hardcoded Credentials in JavaScript oder TypeScript Dateien
- Hardcoded Credentials in PHP Dateien
- Hardcoded Login in JavaScript oder TypeScript Dateien
- Hardcoded Secrets in Perl-Skripten
- Hardcoded Secrets in Python-Skripten
- Hardcoded Secrets in BASH-Skript oder BASH-Setup-Dateien
- HashiCorp Terraform Benutzer/Organisations API Token
- Heroku API Schlüssel
- Hohe Entropie
- HubSpot API Token
- Intercom API Token
- Intercom Client Secret/ID
- Ionic API Token
- JSON Web Token
- Linear API Token
- Linear Client Secret/ID
- LinkedIn Client ID
- LinkedIn Client Secret
- LinkedIn geheimer Schlüssel
- Lob API Schlüssel
- Lob Publishable API Schlüssel
- MailChimp API Schlüssel
- Mailgun API Schlüssel
- Mailgun privater API Token
- Mailgun öffnetlicher validierungs Schlüssel
- Mailgun Webhook Anmeldeschlüssel
- Mapbox API Token
- MessageBird API Client ID
- MessageBird API Token
- Microsoft Teams Webhook
- MongoDB Cloud Connection String
- New Relic Insight Schlüssel
- New Relic REST API Schlüssel
- New Relic Synthetic Locations Schlüssel
- New Relic Admin API Schlüssel
- New Relic Ingest Browser API Token
- New Relic Benutzer API ID
- New Relic Benutzer API Schlüssel
- NuGet API Schlüssel
- PGP privater Schlüssel
- PKCS8 privater Schlüssel
- PayPal
- PayPal Braintree Zugriffs-Token
- Picatic API Schlüssel
- PlanetScale API Token
- PlanetScale Password
- Postman API Token
- Pulumi API Token
- PyPI Upload-Token
- RSA privater Schlüssel
- Riot Spieleentwickler API Schlüssel
- Rubygem API Token
- SSH (DSA) privater Schlüssel
- SSH (EC) privater Schlüssel
- SSH privater Schlüssel
- Secrets in JSON Dateien
- Secrets im XML-Schlüssel des Schlüssel-Werte-Paars
- Secrets im XML-Wert des Schlüssel-Werte-Paars
- SendGrid API Token
- Sendinblue API Token
- Serp API Schlüssel
- Shippo API Token
- Shopify Zugriffs-Token
- Shopify benutzerdefinierter Anwendungs-Zugriffs-Token
- Shopify privater Anwendungs-Zugriffs-Token
- Shopify shared Secret
- Slack Webhook
- Slack Token
- Slack v1 API Token
- Slack v2 API Token
- Square OAuth Secret
- Square persönlicher Zugriffs Token
- Square Zugriffs-Token
- StackHawk API Schlüssel
- Stripe
- Stripe API Schlüssel
- Twilio API Schlüssel
- Twitch API Token
- Twitter Client ID
- Twitter geheimer Schlüssel
- Twitter Token
- Typeform API Token
- URLs of Amazaon AWS S3-Buckets
- US or Canada Zipcode
- WP-Config
- Zapier Webhook
- Zoho Webhook Token
- npm Zugriffs-Token
- YAML Secrets

Quellen auf denen das Regelwerk beruht
------------------------------------------------------------------------------

- https://github.com/zricethezav/gitleaks/blob/master/config/gitleaks.toml
- https://github.com/Plazmaz/leaky-repo
- https://github.com/GSA/odp-code-repository-commit-rules/blob/master/gitleaks/rules.toml
- https://github.com/cloud-gov/caulking/blob/master/local.toml
- https://github.com/Typeform/gitleaks-config/blob/main/global_config.toml
- https://github.com/harry1080/secretx/blob/master/patterns.json
- https://github.com/hisxo/gitGraber/blob/master/tokens.py
- https://github.com/eth0izzle/shhgit/blob/3ce441853d999dacf6e20e59b116c135dcdd0c68/config.yaml
- https://github.com/w-digital-scanner/w13scan/blob/master/W13SCAN/scanners/PerFile/js_sensitive_content.py
- https://github.com/m4ll0k/SecretFinder/blob/master/BurpSuite-SecretFinder/SecretFinder.py
- https://github.com/projectdiscovery/nuclei-templates/blob/master/exposed-tokens/generic/credentials-disclosure.yaml
- https://github.com/udit-thakkur/AdvancedKeyHacks/blob/master/hackcura_apikey_hacks.sh
- https://github.com/gwen001/pentest-tools/blob/master/keyhacks.sh
- https://github.com/hahwul/dalfox/blob/1f32f3494e1aa3312f84b3e2a836eb61a9ae9aac/pkg/scanning/grep.go

.. Seealso::
   Die aktuelle Version ist hier zu finden [442]_ 

.....

.. rubric:: Fussnoten

.. [1]
   Mit Gitleaks Geheimnisse schützen und entdecken
   Protect and discover secrets using Gitleaks
   https://github.com/zricethezav/gitleaks

.. [2]
   GoLang
   https://go.dev/

.. [3]
   GitLeaks installation documentation
   https://github.com/zricethezav/gitleaks#installing

.. [442]
   ndaal - Modifikation zur Suche von Credentials mit Hilfe von GitLeaks
   ndaal - Secrets Search Based on GitLeaks
   https://gitlab.com/ndaal_open_source/ndaal_public_secretes_search
