#!/usr/bin/env bash

# Exit on error. Append "|| true" if you expect an error.
set -o errexit
# Exit on error inside any functions or subshells.
set -o errtrace
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

mydir=$(cd "$(dirname "${0}")"; pwd -P)
BASE_RULESET="${mydir}/${1:-gitleaks_base.toml}"
#PII_RULESET="${mydir}/${2:-gitleaks_pii.toml}"
OUTFILE="${mydir}/${3:-gitleaks_all.toml}"
TMP_FILE="${mydir}/tmp.toml"

rm -f "$TMP_FILE" && touch "$TMP_FILE"

echo "Start merging rules in ${mydir}"
cd "${mydir}"

merge_files=$(find "${mydir}" -type f -name 'gitleaks_*.toml' ! -path "*${BASE_RULESET}" ! -path "*${OUTFILE}")
for merge_file in $merge_files; do
    sed '1,/^# Rules.*#$/d' "$merge_file" | sed -e '1d' -e '/^# Global whitelist.*#$/,$d' | sed '$d' >>"$TMP_FILE"
done

cat "$BASE_RULESET" "$TMP_FILE" >"$OUTFILE"

rm -f "$TMP_FILE"
echo "Done!"
exit 0
